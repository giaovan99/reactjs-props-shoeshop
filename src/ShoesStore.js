import React, { Component } from "react";
import ProductList from "./Components/ProductList";
import Modal from "./Components/Modal";
import { data } from "./Components/Data";

export default class ShoesStore extends Component {
  state = {
    products: data,
    cart: [],
    detail:{},
  };
  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => {
      return item.id == shoe.id;
    });
    // th shoe chưa có trong giỏ hàng -> index=-1
    if (index == -1) {
      let newShoe = { ...shoe, soLuong: 1 };
      cloneCart.push(newShoe);
    }
    // th shoe đã có trong giỏ -> index!=-1 -> tăng số lượng
    else {
      cloneCart[index].soLuong++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDelete = (id) => {
    let newCart = this.state.cart.filter((item) => {
      return item.id != id;
    });
    this.setState({
      cart: newCart,
    });
  };
  handleChangeQuantity = (id, soLuong) => {
    let cloneCart = [...this.state.cart];
    console.log(id);
    let index = cloneCart.findIndex((item) => {
      return item.id == id;
    });
    console.log(index);
    if (index != -1) {
      if (soLuong == -1 && cloneCart[index].soLuong == 1) {
        cloneCart.splice(index);
      } else {
        cloneCart[index].soLuong = cloneCart[index].soLuong + soLuong;
      }
    }
    this.setState({
      cart: cloneCart,
    });
  };
  handleDetail=(shoe,status)=>{
    let newShoe={...shoe};
    this.setState({
        detail:newShoe,
    })
  }
  render() {
    return (
      <div>
        <ProductList
          productsData={this.state.products}
          cart={this.state.cart}
          addToCard={this.handleAddToCart}
          delete={this.handleDelete}
          changeQuantity={this.handleChangeQuantity}
          detail={this.handleDetail}
        ></ProductList>
        {this.state.detail!=''&& <Modal detail={this.state.detail}></Modal>}
      </div>
    );
  }
}
